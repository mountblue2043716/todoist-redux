import React from "react";
import { Modal, Box, Stack } from "@mui/material";

const TaskModal = ({ open, onClose, task, project }) => {
  return (
    <Modal open={open} onClose={onClose}>
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          width: "50%",
          height: "70%",
        }}
      >
        <Stack direction="row" alignItems="center" gap="7px">
          <div
            style={{
              width: "10px",
              height: "10px",
              borderRadius: "50%",
              backgroundColor: project && project.color,
            }}
          ></div>
          <div>{project && project.name}</div>
        </Stack>
        <h2>{task.content}</h2>
        <p>{task.description}</p>
      </Box>
    </Modal>
  );
};

export default TaskModal;
