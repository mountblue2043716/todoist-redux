import React, { useState } from "react";
import { MenuItem, Stack } from "@mui/material";

import { deleteTask, addTask } from "../services/API";
import SnackbarComponent from "./SnackBarComponent";

const EachProject = ({
  project,
  taskId,
  taskContent,
  taskDescription,
  projectId,
  handleMoveMenuClose,
  handleMenuClose,
  setTasks,
}) => {
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [severity, setSeverity] = useState("error");

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleMove = (taskId, taskContent, taskDescription, projectId) => {
    handleMoveMenuClose();
    handleMenuClose();
    addTask(projectId, taskContent, taskDescription)
      .then(() => {
        setMessage("Task moved");
        setSeverity("success");
        setSnackbarOpen(true);
      })
      .catch((error) => {
        console.error("Error moving task:", error);
        setMessage("Failed to move task.");
        setSnackbarOpen(true);
      });
    deleteTask(taskId)
      .then((response) => {
        setTasks((prevTasks) => prevTasks.filter((item) => item.id !== taskId));
      })
      .catch((error) => {
        console.error("Error moving task:", error);
        setMessage("Failed to move task.");
        setSnackbarOpen(true);
      });
  };

  return (
    <>
      <MenuItem
        onClick={(e) =>
          handleMove(taskId, taskContent, taskDescription, projectId)
        }
      >
        <Stack direction="row" alignItems="center" gap="7px">
          <div
            style={{
              width: "10px",
              height: "10px",
              borderRadius: "50%",
              backgroundColor: project && project.color,
            }}
          ></div>
          <div>{project && project.name}</div>
        </Stack>
      </MenuItem>
      <SnackbarComponent
        open={snackbarOpen}
        message={message}
        severity={severity}
        onClose={handleSnackbarClose}
      />
    </>
  );
};

export default EachProject;
