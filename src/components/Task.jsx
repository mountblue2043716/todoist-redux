import React, { useState } from "react";
import {
  ListItem,
  Checkbox,
  Button,
  Menu,
  MenuItem,
  Stack,
  Typography,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import DriveFileMoveIcon from "@mui/icons-material/DriveFileMove";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import dayjs from "dayjs";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DateCalendar } from "@mui/x-date-pickers/DateCalendar";

import EachProject from "./EachProject";
import AlertDialog from "./DeleteConfirmation";
import CreateAndEditTaskForm from "./CreateAndEditTaskForm";
import { deleteTask, markTaskCompleted, updateTask } from "../services/API";
import SnackbarComponent from "./SnackBarComponent";
import TaskModal from "./TaskModal";

const Task = ({
  task,
  editTaskId,
  projects,
  projectId,
  setEditTaskId,
  handleSubmit,
  newTaskName,
  newTaskDescription,
  expandCollapse,
  setExpanded,
  tasks,
  setNewTaskName,
  setNewTaskDescription,
  setTasks,
  project,
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [openConfirmation, setOpenConfirmation] = React.useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [nestedAnchorEl, setNestedAnchorEl] = useState(null);
  const [dateAnchorEl, setDateAnchorEl] = useState(null);
  // let date = new Date();
  // let date1 = `${date.$y}-${date.$M}-${date.$D}`
  let date = task?.due?.date || "2023-06-24";
  const [newDate, setNewDate] = useState(dayjs(date));
  const handleOpenConfirmation = (e) => {
    e.stopPropagation();
    e.preventDefault();
    setOpenConfirmation(true);
    handleMenuClose();
  };
  const handleCloseConfirmation = () => {
    setOpenConfirmation(false);
  };

  const handleTaskCheck = (taskId) => {
    markTaskCompleted(taskId)
      .then((response) => {
        setTasks((prevTasks) =>
          prevTasks.filter((task) => task?.id !== taskId)
        );
      })
      .catch((error) => {
        console.error("Error:", error);
        setErrorMessage("Failed to mark task as complete.");
        setSnackbarOpen(true);
      });
  };

  const handleEditTask = (task) => {
    setEditTaskId(task?.id);
    setExpanded(false);
    setNewTaskName(task?.content);
    setNewTaskDescription(task?.description);
    handleMenuClose();
  };

  const handleDeleteTask = async (taskId) => {
    deleteTask(taskId)
      .then((response) => {
        setTasks(tasks.filter((task) => task?.id !== taskId));
      })
      .catch((error) => {
        console.error("Error deleting task:", error);
        setErrorMessage("Failed to delete task?.");
        setSnackbarOpen(true);
      });
  };

  const handleSetDate = (date) => {
    handleDateMenuClose();
    handleMenuClose();
    setNewDate(date);
    const dateString = `${date.$y}-${date.$M + 1}-${date.$D}`;
    updateTask(task?.id, task?.name, task?.description, date)
      .then(() => {
        setTasks(
          tasks.map((item) => {
            return task?.id === item.id
              ? { ...item, due: { ...item.due, date: dateString } }
              : item;
          })
        );
      })
      .catch((error) => {
        console.error("Error updating task:", error);
        setErrorMessage("Failed to update task?.");
        setSnackbarOpen(true);
      });
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };
  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  const handleMoveMenuOpen = (event) => {
    setNestedAnchorEl(event.currentTarget);
  };
  const handleMoveMenuClose = () => {
    setNestedAnchorEl(null);
  };
  const handleDateMenuOpen = (event) => {
    setDateAnchorEl(event.currentTarget);
  };
  const handleDateMenuClose = () => {
    setDateAnchorEl(null);
  };

  return (
    <>
      <ListItem
        onClick={() => setOpenModal(true)}
        dense
        sx={{ display: "flex", justifyContent: "space-between" }}
      >
        {editTaskId === task?.id ? (
          <CreateAndEditTaskForm
            handleSubmit={handleSubmit}
            newTaskName={newTaskName}
            setNewTaskName={setNewTaskName}
            newTaskDescription={newTaskDescription}
            setNewTaskDescription={setNewTaskDescription}
            expandCollapse={expandCollapse}
            submitButtonText="Save"
          />
        ) : (
          <>
            <Checkbox
              checked={false}
              onChange={() => handleTaskCheck(task?.id)}
              inputProps={{ "aria-label": "checkbox" }}
            />
            <Stack width="100%">
              <Typography fontSize="18px">{task?.content}</Typography>
              <Typography color="gray">{task?.description}</Typography>
              {task?.due && (
                <Typography color="gray">
                  <CalendarMonthIcon
                    fontSize="14px"
                    sx={{ marginRight: "4px" }}
                  />
                  {task?.due?.date}
                </Typography>
              )}
              <div>
                <hr style={{ borderColor: "rgba(220, 214, 208, 0.2)" }} />
              </div>
            </Stack>
            <div>
              <Button onClick={handleMenuOpen}>
                <MoreHorizIcon />
              </Button>
              <Menu
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleMenuClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                <MenuItem onClick={() => handleEditTask(task)}>
                  <EditIcon fontSize="small" sx={{ marginRight: "10px" }} />
                  Edit task
                </MenuItem>
                <MenuItem onClick={handleDateMenuOpen}>
                  <CalendarMonthIcon
                    fontSize="small"
                    sx={{ marginRight: "10px" }}
                  />
                  Set due date
                </MenuItem>
                <MenuItem onClick={handleMoveMenuOpen}>
                  <DriveFileMoveIcon
                    fontSize="small"
                    sx={{ marginRight: "10px" }}
                  />
                  Move task
                </MenuItem>
                <MenuItem onClick={(e) => handleOpenConfirmation(e)}>
                  <DeleteIcon fontSize="small" sx={{ marginRight: "10px" }} />
                  Delete task
                </MenuItem>
              </Menu>
              <Menu
                anchorEl={nestedAnchorEl}
                open={Boolean(nestedAnchorEl)}
                onClose={handleMoveMenuClose}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
              >
                {projects
                  .filter(
                    (project) =>
                      project.id !== projectId && project.name !== "Inbox"
                  )
                  .map((project) => (
                    <EachProject
                      key={project.id}
                      project={project}
                      taskId={task?.id}
                      taskContent={task?.content}
                      taskDescription={task?.description}
                      projectId={project.id}
                      handleMoveMenuClose={handleMoveMenuClose}
                      handleMenuClose={handleMenuClose}
                      setTasks={setTasks}
                    />
                  ))}
              </Menu>
              <Menu
                anchorEl={dateAnchorEl}
                open={Boolean(dateAnchorEl)}
                onClose={handleDateMenuClose}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
              >
                {/* <MenuItem>Today</MenuItem>
                <MenuItem>Tomorrow</MenuItem>
                <MenuItem>This weekend</MenuItem> */}
                <MenuItem>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DemoContainer components={["DateCalendar"]}>
                      <DemoItem>
                        <DateCalendar
                          value={newDate}
                          onChange={(date) => handleSetDate(date)}
                        />
                      </DemoItem>
                    </DemoContainer>
                  </LocalizationProvider>
                </MenuItem>
              </Menu>
            </div>
          </>
        )}
      </ListItem>
      {/* <TaskModal
        open={openModal}
        onClose={() => setOpenModal(false)}
        task={task}
        project={project}
      /> */}
      <AlertDialog
        handleDelete={handleDeleteTask}
        open={openConfirmation}
        handleClose={handleCloseConfirmation}
        taskId={task?.id}
        name={task?.content}
        type="task"
      />
      <SnackbarComponent
        open={snackbarOpen}
        message={errorMessage}
        onClose={handleSnackbarClose}
      />
    </>
  );
};

export default Task;
