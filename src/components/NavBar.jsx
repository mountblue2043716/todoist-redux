import { CssBaseline, Toolbar, Typography, AppBar } from "@mui/material";

const NavBar = () => {
  return (
    <>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{ marginBottom: "30px", backgroundColor: "#DB4C3F" }}
      >
        <Toolbar>
          <Typography variant="h4">Todoist</Typography>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default NavBar;
