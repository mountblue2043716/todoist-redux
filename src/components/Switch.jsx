import * as React from "react";
import Switch from "@mui/material/Switch";

export default function MySwitch({ setIsFavorite, isFavorite }) {
  const [checked, setChecked] = React.useState(false);

  const handleChange = (event) => {
    setChecked(event.target.checked);
    setIsFavorite((prevState) => !prevState);
  };

  return (
    <Switch
      checked={isFavorite ? true : checked}
      onChange={handleChange}
      inputProps={{ "aria-label": "controlled" }}
    />
  );
}
