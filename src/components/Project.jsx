import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { List, Button, Box, Collapse } from "@mui/material";

import SnackbarComponent from "./SnackBarComponent";
import CreateAndEditTaskForm from "./CreateAndEditTaskForm";
import Task from "./Task";
import { getTasks, addTask, updateTask, getAllProjects } from "../services/API";
import { useDispatch, useSelector } from "react-redux";

const Project = () => {
  const projects = useSelector((state) => state.projects);

  const { projectId } = useParams();
  const [tasks, setTasks] = useState([]);
  // const [projects, setProjects] = useState([]);
  const [newTaskName, setNewTaskName] = useState("");
  const [newTaskDescription, setNewTaskDescription] = useState("");
  const [editTaskId, setEditTaskId] = useState("");
  const [expanded, setExpanded] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  useEffect(() => {
    fetchTasks();
    // fetchProjects();
  }, [projectId]);

  const fetchTasks = async () => {
    getTasks(projectId)
      .then((response) => {
        setTasks(response);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  // const fetchProjects = async () => {
  //   getAllProjects()
  //     .then((response) => {
  //       setProjects(response);
  //     })
  //     .catch((error) => {
  //       console.error("Error:", error);
  //       setErrorMessage("Failed to fetch projects.");
  //       setSnackbarOpen(true);
  //     });
  // };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (editTaskId) {
      updateTask(editTaskId, newTaskName, newTaskDescription)
        .then((response) => {
          setTasks(
            tasks.map((task) => (editTaskId === task.id ? response : task))
          );
          setEditTaskId("");
          setNewTaskName("");
          setNewTaskDescription("");
        })
        .catch((error) => {
          console.error("Error updating task:", error);
          setErrorMessage("Failed to update task.");
          setSnackbarOpen(true);
        });
    } else {
      addTask(projectId, newTaskName, newTaskDescription)
        .then((response) => {
          setTasks((prevState) => [...prevState, response]);
          setNewTaskName("");
          setNewTaskDescription("");
          setExpanded(false);
        })
        .catch((error) => {
          console.error("Error adding task:", error);
          setErrorMessage("Failed to add task.");
          setSnackbarOpen(true);
        });
    }
  };

  const expandCollapse = (booleanValue) => {
    setExpanded(booleanValue);
    setEditTaskId("");
    setNewTaskName("");
    setNewTaskDescription("");
  };

  const project = projects.find((project) => project.id === projectId);

  return (
    <Box sx={{ padding: "100px", paddingLeft: "200px" }}>
      <h2>{project && project.name}</h2>
      <List sx={{ width: "40vw" }}>
        {tasks.map((task) => (
          <Task
            key={task?.id}
            task={task}
            editTaskId={editTaskId}
            projects={projects}
            projectId={projectId}
            setEditTaskId={setEditTaskId}
            handleSubmit={handleSubmit}
            newTaskName={newTaskName}
            newTaskDescription={newTaskDescription}
            expandCollapse={expandCollapse}
            setExpanded={setExpanded}
            tasks={tasks}
            setNewTaskName={setNewTaskName}
            setNewTaskDescription={setNewTaskDescription}
            setTasks={setTasks}
            project={project}
          />
        ))}
      </List>
      <Box>
        <div style={{ marginTop: "10px" }}>
          <Button onClick={()=>expandCollapse(true)}>{expanded ? "" : "+ Add task"}</Button>
          <Collapse in={expanded}>
            <CreateAndEditTaskForm
              handleSubmit={handleSubmit}
              newTaskName={newTaskName}
              setNewTaskName={setNewTaskName}
              newTaskDescription={newTaskDescription}
              setNewTaskDescription={setNewTaskDescription}
              expandCollapse={expandCollapse}
              submitButtonText="Add task"
            />
          </Collapse>
        </div>
      </Box>
      <SnackbarComponent
        open={snackbarOpen}
        message={errorMessage}
        onClose={() => setSnackbarOpen(false)}
      />
    </Box>
  );
};

export default Project;
