import React, { useState, useEffect } from "react";
// import { getAllTasks } from "../services/API";
// import { List, ListItem} from "@mui/material";

const Home = () => {
  // const [tasks, setTasks] = useState([]);
  // useEffect(() => {
  //   fetchAllTasks();
  // }, []);
  // // console.log(tasks)

  // const fetchAllTasks = () => {
  //   getAllTasks()
  //     .then((response) => {
  //       setTasks(response);
  //     })
  //     .catch((error) => {
  //       console.error("Error:", error);
  //     });
  // };

  return (
    <div
      style={{
        width: "80%",
        display: "flex",
        justifyContent: "center",
      }}
    >
      {/* <List sx={{marginTop:"2 00px"}}>
      {tasks
        ?.filter((task) => task?.due?.date === '2023-06-22')
        .map((task) => (
          <ListItem key={task.id}>{task.content}</ListItem>
        ))}
      </List> */}

      <div
        style={{ textAlign: "center", marginTop: "250px", fontSize: "26px" }}
      >
        Welcome <br />
        to <br />
        Todoist
      </div>
    </div>
  );
};

export default Home;
