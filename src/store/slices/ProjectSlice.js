import { createSlice } from "@reduxjs/toolkit";

const ProjectSlice = createSlice({
  name: "project",
  initialState: [],
  reducers: {
    setProjects: (state, action) => {
      return (state = action.payload);
    },
    createProject: (state, action) => {
      state.push(action.payload);
    },
    removeProject: (state, action) => {
      return state.filter((project) => project.id !== action.payload);
    },
    editProject: (state, action) => {
      const response = action.payload;
      return state.map((project) =>
        project.id === response.id ? response : project
      );
    },
    setFavorite: (state, action) => {
      return state.map((project) =>
        project.id === action.payload.id
          ? { ...project, is_favorite: !project.is_favorite }
          : project
      );
    },
  },
});

export const {
  setProjects,
  createProject,
  removeProject,
  editProject,
  setFavorite,
} = ProjectSlice.actions;
export default ProjectSlice.reducer;
