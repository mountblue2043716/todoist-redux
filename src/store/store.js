import { configureStore } from "@reduxjs/toolkit";
import ProjectSlice from "./slices/ProjectSlice.js";


const store = configureStore({
  reducer: {
    projects: ProjectSlice,
  },
});

export default store;
